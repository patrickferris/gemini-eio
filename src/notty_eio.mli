open Eio.Std

module Term : sig
  type t

  val create : ?nosig:bool ->
               ?mouse:bool ->
               ?bpaste:bool ->
               sw:Switch.t ->
               input:Eio.Flow.source ->
               output:Eio.Flow.sink ->
               unit -> t

  val image : t -> Notty.image -> unit
  val refresh : t -> unit
  val cursor : t -> (int * int) option -> unit
  val event : t -> [ Notty.Unescape.event | `Resize ]
  val size : t -> int * int
end
