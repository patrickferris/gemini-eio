open Astring
open Eio.Std

let src = Logs.Src.create "gemini-protocol" ~doc:"Gemini protocol library"
module Log = (val Logs.src_log src : Logs.LOG)

let tls_config =
  Mirage_crypto_rng_unix.initialize ();
  let null ?ip:_ ~host:_ _certs = Ok None in
  Tls.Config.client ~authenticator:null ()      (* todo: TOFU *)

type t = {
  net : Eio.Net.t;
}

let create net =
  { net }

type response =
  | OK of { url : Uri.t; mime_type : string; data : string }

let response =
  let open Eio.Buf_read.Syntax in
  let* code = Eio.Buf_read.take 2 in
  match code.[0] with
  | '2' ->
    let+ mime_type = Eio.Buf_read.(char ' ' *> line)
    and+ body = Eio.Buf_read.take_all in
    `OK (mime_type, body)
  | '3' ->
    let+ uri = Eio.Buf_read.(char ' ' *> line) in
    `Redirect (Uri.of_string uri)
  | _ -> Fmt.failwith "Unsupported status code %S" code

let rec get t url =
  if Uri.scheme url <> Some "gemini" then Fmt.failwith "Not a gemini URL: %a" Uri.pp url;
  let host, ip =
    match Uri.host url with
    | None -> Fmt.failwith "Missing host in URL %a" Uri.pp url
    | Some host ->
      match (Unix.gethostbyname host).h_addr_list with
      | [| |] -> Fmt.failwith "Unknown host %S in URL %a" host Uri.pp url
      | arr -> host, arr.(0)
      | exception Not_found -> Fmt.failwith "Unknown host %S in URL %a" host Uri.pp url
  in
  let port = Uri.port url |> Option.value ~default:1965 in
  let addr = `Tcp (Eio_unix.Ipaddr.of_unix ip, port) in
  Log.debug (fun f -> f "Connecting to %a (%a)" Uri.pp url Eio.Net.Sockaddr.pp addr);
  let response =
    Switch.run @@ fun sw ->
    let conn = Eio.Net.connect ~sw t.net addr in
    let host = Domain_name.of_string_exn host |> Domain_name.host |> Result.to_option in
    let conn = Tls_eio.client_of_flow tls_config ?host conn in
    Eio.Flow.copy_string (Uri.to_string url ^ "\r\n") conn;
    Eio.Buf_read.parse_exn response conn ~max_size:(1024 * 1024)
  in
  match response with
  | `OK (mime_type, data) ->
    Log.debug (fun f -> f "OK: mime-type=%S" mime_type);
    OK { url; mime_type; data }
  | `Redirect uri ->
    Log.debug (fun f -> f "Redirect to %a" Uri.pp uri);
    get t uri
