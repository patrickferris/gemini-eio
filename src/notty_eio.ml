open Eio.Std

open Notty

module Term = struct
  type t = {
    trm     : Tmachine.t;
    input   : Eio.Flow.source;
    ibuf    : Cstruct.t;                (* Scratch buffer for [event] *)
    iparse  : Unescape.t;               (* Input parser *)
    output  : Eio.Flow.sink;
    obuf    : Buffer.t;                 (* Pending output *)
    cleanup : (unit -> unit) Queue.t;   (* Actions to perform on release *)
    mutable need_resize : bool;
    mutable hook : Eio.Switch.hook;
  }

  let write t =
    Tmachine.output t.trm t.obuf;
    let out = Buffer.contents t.obuf in (* XXX There goes 0copy. :/ *)
    Buffer.clear t.obuf;
    Eio.Flow.copy_string out t.output

  let release t =
    Eio.Switch.remove_hook t.hook;
    Queue.iter (fun f -> f ()) t.cleanup;
    if Tmachine.release t.trm then write t

  let or_fail msg = function
    | None -> failwith msg
    | Some x -> x

  let unix_file_descr fd =
    Eio_unix.FD.peek_opt fd |> or_fail "Not a file-descriptor!"

  let cap_for_fd x = Notty_unix.Private.cap_for_fd (unix_file_descr x)

  let refresh t      = Tmachine.refresh t.trm    ; write t
  let image t image  = Tmachine.image t.trm image; write t
  let cursor t curs  = Tmachine.cursor t.trm curs; write t
  let set_size t dim = Tmachine.set_size t.trm dim
  let size t         = Tmachine.size t.trm

  let on_resize t () =
    (* todo: send a notification to the main event loop too *)
    t.need_resize <- true

  let with_cleanup t (`Revert fn) =
    Queue.add fn t.cleanup

  let create ?(nosig=true) ?(mouse=true) ?(bpaste=true) ~sw ~input ~output () =
    let t = {
      trm = Tmachine.create ~mouse ~bpaste (cap_for_fd output);
      input;
      ibuf = Cstruct.create 1024;
      iparse = Unescape.create ();
      output;
      obuf = Buffer.create 4096;
      hook = Eio.Switch.null_hook;
      cleanup = Queue.create ();
      need_resize = false;
    }
    in
    t.hook <- Switch.on_release_cancellable sw (fun () -> release t);
    with_cleanup t @@ Notty_unix.Private.setup_tcattr ~nosig (unix_file_descr input);
    with_cleanup t @@ Notty_unix.Private.set_winch_handler (on_resize t);
    Notty_unix.winsize (unix_file_descr output) |> Option.iter (set_size t);
    t

  let rec event t =
    if t.need_resize then (
      t.need_resize <- false;
      Buffer.reset t.obuf;
      Notty_unix.winsize (unix_file_descr t.output) |> Option.iter (set_size t);
      `Resize
    ) else match Unescape.next t.iparse with
      | #Unescape.event as r -> r
      | `End   -> raise End_of_file
      | `Await ->
        let len = Eio.Flow.read t.input t.ibuf in
        let ibuf = Cstruct.to_bytes t.ibuf ~len in
        Unescape.input t.iparse ibuf 0 len;
        event t
end
