open Eio.Std
open Astring

let default_uri = Uri.of_string "gemini://gemini.circumlunar.space/"

exception Cancelled

let parse_args = function
  | [_] -> default_uri
  | [_; url] -> Uri.of_string url
  | _ -> failwith "usage: gemini-eio URL"

module Links = struct
  type t = {
    mutable links : Uri.t list;
    mutable next : int;
  }

  let create () =
    { links = []; next = 0 }

  let add t link =
    t.links <- link :: t.links;
    let i = t.next in
    t.next <- succ i;
    i

  let get t n =
    let i = t.next - 1 - n in
    if i >= 0 then List.nth_opt t.links i
    else None
end

module Cli = struct
  open Eio.Buf_read
  open Eio.Buf_read.Syntax

  let is_command_char = function
    | ' ' | '\n' -> false
    | _ -> true

  let is_space = function
    | ' ' | '\t' -> true
    | _ -> false

  let spaces = skip_while is_space

  let command =
    let* c = spaces *> take_while is_command_char <* spaces in
    match c with
    | ""                 -> Fun.const `Help
    | "goto" | "g"       ->
      let+ url = take_while ((<>) '\n') in
      begin match String.trim url with
        | "" -> `Error "Missing URL"
        | url -> `Goto (Uri.of_string url)
      end
    | x ->
      match int_of_string_opt x with
      | Some x -> Fun.const (`Link x)
      | None -> Fun.const (`Error (Fmt.str "Unknown command %S" x))

  let parse line =
    parse_exn command (Eio.Flow.string_source line) ~max_size:max_int
end

type minibuffer = {
  text : string;
}

type event = [ Notty.Unescape.event | `Redraw | `Resize ]

type t = {
  term : Notty_eio.Term.t;
  sw : Switch.t;
  command : event Eio.Stream.t;
  mutable minibuffer : minibuffer option;
  mutable page : Notty.I.t;
  mutable scroll : int;
  mutable message : Notty.I.t;
  proto : Protocol.t;
  mutable links : Links.t;
  mutable url : Uri.t;
  mutable history : Uri.t list;
  mutable fetch_sw : Switch.t option;        (* Switch for the currently-active fetch *)
}

(* Notty chokes on control characters *)
let sanitise =
  String.map (function
      | '\x00'..'\x1f' | '\x7f' -> '?'
      | c -> c
    )

let render_page t data =
  let g = Gemini.create t.url in
  let data = String.drop ~rev:true ~max:1 ~sat:((=) '\n') data in
  let lines = String.cuts ~sep:"\n" data in
  let links = Links.create () in
  t.page <- Notty.I.empty;
  let (screen_width, _) = Notty_eio.Term.size t.term in
  lines |> List.iter (fun line ->
      let module I = Notty.I in
      let image =
        match Gemini.input g line with
        | `Blank
        | `Fence _     -> I.string Notty.A.empty ""
        | `Verbatim x  -> I.strf "@[<h>%s@]" (sanitise x)
        | `Text x      -> I.strf ~w:screen_width "@[%a@]" Fmt.text (sanitise x)
        | `Link (link, None) ->
          let n = Links.add links link in
          I.strf "@[<h>%2d => <%a>@]" n Uri.pp link;
        | `Link (link, Some desc) ->
          let n = Links.add links link in
          I.strf "@[<h>%2d => %s@]" n desc;
          (* Fmt.pf f "@[<h>   <%a>@]" Uri.pp link; *)
      in
      t.page <- Notty.Infix.(t.page <-> image)
    );
  t.links <- links

let box t fmt =
  let open Notty.Infix in
  let (w, _) = Notty_eio.Term.size t.term in
  let a = Notty.A.(bg lightblue ++ fg black) in
  Notty.I.kstrf ~attr:a ~w:(w - 4) (fun i ->
      let w = Notty.I.width i + 4 in
      let h = Notty.I.height i + 2 in
      let bg = Notty.I.char a ' ' w h in
      (Notty.I.pad ~l:2 ~t:1 i </> bg)
    )
    fmt

let render t (w, h) =
  let open Notty in
  let open Notty.Infix in
  let title =
    let url = Uri.to_string t.url in
    I.string A.(st reverse) (url ^ Stdlib.String.make (max 0 (w - String.length url)) ' ')
  in
  let minibuffer =
    match t.minibuffer with
    | None -> I.empty
    | Some { text } ->
      I.string A.empty ("> " ^ text)
  in
  let main =
    I.vsnap ~align:`Top (h - 1 - I.height minibuffer) (I.vcrop t.scroll 0 t.page) in
  let message =
    t.message
    |> I.vsnap h
    |> I.hsnap w
  in
  message </> (title <-> main <-> minibuffer)

let update_display t =
  let (w, h) = Notty_eio.Term.size t.term in
  Notty_eio.Term.image t.term (render t (w, h));
  let cursor =
    match t.minibuffer with
    | None -> None
    | Some { text } -> Some (String.length text + 2, h - 1)
  in
  Notty_eio.Term.cursor t.term cursor

let page_step t =
  let (_, h) = Notty_eio.Term.size t.term in
  h - 3

let ignore_cancelled = function
  | Cancelled -> ()
  | ex -> raise ex

let rec cli t =
  t.minibuffer <- None;
  let rec get_command () =
    update_display t;
    let event = Eio.Stream.take t.command in
    match t.minibuffer with
    | Some mb -> handle_minibuffer_event mb event
    | None -> handle_page_event event
  and handle_minibuffer_event mb = function
    | `Key (`Enter, _) -> Cli.parse mb.text
    | `Key (`Backspace, _) when mb.text <> "" ->
      let mb = {text = String.with_range mb.text ~len:(String.length mb.text - 1)} in
      t.minibuffer <- Some mb;
      get_command ()
    | `Key (`ASCII c, _) ->
      let text = mb.text ^ String.of_char c in
      t.minibuffer <- Some { text };
      get_command ()
    | event -> handle_page_event event
  and handle_page_event = function
    | `Key (`Escape, _) ->
      Option.iter (fun sw -> Switch.fail sw Cancelled; render_page t "Cancelled") t.fetch_sw;
      t.message <- Notty.I.empty; t.minibuffer <- None; get_command ()
    | `Key (`Arrow `Up, _) -> t.scroll <- max 0 (t.scroll - 1); get_command ()
    | `Key (`Arrow `Down, _) -> t.scroll <- min (Notty.I.height t.page - 1) (t.scroll + 1); get_command ()
    | `Key (`Home, _) -> t.scroll <- 0; get_command ()
    | `Key (`End, _) -> t.scroll <- Notty.I.height t.page - page_step t; get_command ()
    | `Key (`Page `Up, _) -> t.scroll <- max 0 (t.scroll - (page_step t)); get_command ()
    | `Key (`Page `Down, _) -> t.scroll <- min (Notty.I.height t.page - 1) (t.scroll + (page_step t)); get_command ()
    | `Key (`Function 5, _) -> `Refresh
    | `Key (`ASCII 'C', [`Ctrl]) -> `Quit
    | `Key (`ASCII 'q', _) -> `Quit
    | `Key (`ASCII 'b', _) -> `Back
    | `Key (`ASCII ('h' | '?'), _) -> `Help
    | `Key (`ASCII 'g', _) ->
      t.minibuffer <- Some { text = "goto " };
      get_command ()
    | `Key (`ASCII ('0'..'9' as c), _) ->
      let text = String.of_char c in
      t.minibuffer <- Some { text };
      get_command ()
    | _ -> get_command ()
  in
  match get_command () with
  | `Quit -> `Quit
  | `Back -> back t
  | `Refresh -> look t
  | `Help ->
    t.message <- box t
        "@[<v>Enter a link number to follow it, or:@,\
         - [b]ack     : go back@,\
         - [q]uit     : quit@,\
         - [g]oto URL : go to URL@,\
         - [l]ook     : look at page again@]";
    cli t
  | `Link i -> link t i
  | `Goto x -> goto t x
  | `Error x -> traceln "Error: %s" x; cli t
and back t =
  match t.history with
  | [] -> traceln "No history!"; cli t
  | x :: xs ->
    t.history <- xs;
    t.url <- x;
    look t
and look t =
  t.page <- Notty.(I.string A.empty "Fetching...");
  t.scroll <- 0;
  Option.iter (fun sw -> Switch.fail sw Cancelled) t.fetch_sw;
  Fiber.fork_sub ~sw:t.sw ~on_error:ignore_cancelled
    (fun fetch_sw ->
       let some_fetch_sw = Some fetch_sw in
       t.fetch_sw <- some_fetch_sw;
       begin
         try
           match Protocol.get t.proto t.url with
           | Protocol.OK { url; mime_type = _; data } ->
             t.url <- url;
             render_page t data
         with
         | Failure msg -> render_page t @@ Fmt.str "@[Error:@ %s@]" msg
         | ex ->
           Fiber.check ();      (* Abort if we're being cancelled *)
           render_page t @@ Fmt.str "@[Error:@ %a@]" Fmt.exn ex
       end;
       t.scroll <- 0;
       t.fetch_sw <- None;
       Eio.Stream.add t.command `Redraw
    );
  cli t
and link t i =
  match Links.get t.links i with
  | None -> traceln "Invalid link %d" i; cli t
  | Some link -> goto t link
and goto t url =
  t.history <- t.url :: t.history;
  t.url <- url;
  look t

let notty_traceln t =
  { Eio.Debug.traceln = fun ?__POS__:_ fmt ->
        let attr = Notty.A.(bg red ++ fg white ++ st bold) in
        Notty.I.kstrf ~attr (fun i ->
            let open Notty.Infix in
            t.message <- t.message <-> i
          ) fmt
  }

let main ~debug ~net ~stdin ~stdout argv =
  Switch.run @@ fun sw ->
  let url = parse_args argv in
  let term = Notty_eio.Term.create ~sw ~input:stdin ~output:stdout ~mouse:false () in
  let proto = Protocol.create net in
  let page = Notty.(I.string A.empty "Ready") in
  let command = Eio.Stream.create 0 in
  let t = {
    sw;
    proto;
    url;
    links = Links.create ();
    history = [];
    term;
    command;
    minibuffer = None;
    page;
    scroll = 0;
    message = Notty.I.empty;
    fetch_sw = None;
  } in
  Fiber.with_binding debug#traceln (notty_traceln t) @@ fun () ->
  Fiber.fork ~sw (fun () ->
      while true do
        (Notty_eio.Term.event t.term :> event)
        |> Eio.Stream.add t.command
      done
    );
  update_display t;
  let `Quit = look t in
  raise Exit            (* Will terminate the other fibres *)

let () =
  Eio_main.run @@ fun env ->
  try
    main (Array.to_list Sys.argv)
      ~debug:(Eio.Stdenv.debug env)
      ~net:(Eio.Stdenv.net env)
      ~stdin:(Eio.Stdenv.stdin env)
      ~stdout:(Eio.Stdenv.stdout env)
  with Exit -> ()
